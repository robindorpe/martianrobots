# Martian Robots

Martian Robots is a Swift implementation of the martian robots challenge

## Installation

Just clone the repository and run it on a simulator. Don't run it on a device or you won't see any output.
You need an iPhone simulator with an iOS version of at least 13.0

## Usage

In a production app, the grid's top right corner should be entered by the user, but with time limitation it's stored as a constant in `ViewController`.

To modify it simply update the `x` and `y` coordinates of:
```
static let topRightCorner: Coordinate = Coordinate(x: 5, y: 3)
```

The `X` text field is to enter the robot x coordinate.

The `Y` text field is to enter the robot y coordinate.

The `🧭` text field is to enter the robot orientation. It has a picker view as input view

The big bottom text field is to enter the instructions as a continuous string. It has a custom view as input view.

The custom view works as follow:

⬅️ Rotate robot to the left.

⬆️ Move robot forward.

➡️ Rotate robot to the right.

The delete button removes the last instruction.

Once done entering the robot coordinate, orientation and instructions tap `Add robot` to validate the changes. If some information is missing, an error popup will be presented to the user.

`Remove all robots` will delete all robots and clear all robot "scents"

Once happy with the added robots, press `Compute instructions`

The result of the computation will be displayed in the console.

Unit tests have been added to the project and can be run in Xcode.

(Please note that robot "scents" and robots are not cleared unless you tap the remove button. So computing the same instructions with the same robots twice may yield different result)