//
//  MartianRobotsTests.swift
//  MartianRobotsTests
//
//  Created by Robin Dorpe on 09/03/2020.
//  Copyright © 2020 Coding Cube Ltd. All rights reserved.
//

import XCTest
@testable import MartianRobots

class MartianRobotsTests: XCTestCase {

    func testSampleRobotInstructions() {
        let robotManager = RobotManager(with: Coordinate(x: 5, y: 3))
        robotManager.add(robot: Robot(coordinate: Coordinate(x: 1, y: 1), orientation: .east),
                         withInstructions: "RFRFRFRF")
        robotManager.add(robot: Robot(coordinate: Coordinate(x: 3, y: 2), orientation: .north),
                         withInstructions: "FRRFLLFFRRFLL")
        robotManager.add(robot: Robot(coordinate: Coordinate(x: 0, y: 3), orientation: .west),
                         withInstructions: "LLFFFLFLFL")
        
        let robotsPosition = robotManager.computeInstructions()
        let sampleRobotsPosition = "1 1 E\n3 3 N LOST\n2 3 S"
        
        // we should have the same output as the sample found in the assignment description
        XCTAssertEqual(robotsPosition, sampleRobotsPosition)
    }
    
    func testNegativeXInstructions() {
        let robotManager = RobotManager(with: Coordinate(x: 50, y: 50))
        robotManager.add(robot: Robot(coordinate: Coordinate(x: 0, y: 0), orientation: .north),
                         withInstructions: "FLFRRFF")
        let robotsPosition = robotManager.computeInstructions()
        let sampleRobotsPosition = "0 1 W LOST"
        XCTAssertEqual(robotsPosition, sampleRobotsPosition)
    }
    
    func testNegativeYInstructions() {
        let robotManager = RobotManager(with: Coordinate(x: 50, y: 50))
        robotManager.add(robot: Robot(coordinate: Coordinate(x: 0, y: 0), orientation: .east),
                         withInstructions: "FRFLLFF")
        let robotsPosition = robotManager.computeInstructions()
        let sampleRobotsPosition = "1 0 S LOST"
        XCTAssertEqual(robotsPosition, sampleRobotsPosition)
    }

}
