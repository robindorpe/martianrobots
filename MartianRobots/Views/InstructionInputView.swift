//
//  InstructionInputView.swift
//  MartianRobots
//
//  Created by Robin Dorpe on 10/03/2020.
//  Copyright © 2020 Coding Cube Ltd. All rights reserved.
//

import UIKit

protocol InstructionInputViewDelegate: AnyObject {
    func instructionInputView(_ view: InstructionInputView,
                              didChooseDirection direction: Direction)
    func instructionInputViewDidChooseDelete(_ view: InstructionInputView)
}

class InstructionInputView: UIView {
    
    weak var delegate: InstructionInputViewDelegate?
    
    @IBAction func moveForward(_ sender: UIButton) {
        delegate?.instructionInputView(self, didChooseDirection: .forward)
    }
    
    @IBAction func moveLeft(_ sender: UIButton) {
        delegate?.instructionInputView(self, didChooseDirection: .left)
    }
    
    @IBAction func moveRight(_ sender: UIButton) {
        delegate?.instructionInputView(self, didChooseDirection: .right)
    }
    
    @IBAction func deleteLast(_ sender: UIButton) {
        delegate?.instructionInputViewDidChooseDelete(self)
    }
    
}
