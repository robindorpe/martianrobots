//
//  RobotManager.swift
//  MartianRobots
//
//  Created by Robin Dorpe on 10/03/2020.
//  Copyright © 2020 Coding Cube Ltd. All rights reserved.
//

import Foundation

// RobotPosition has the same variables than Robot but they are different
// in a prod app Robot would most likely have more variable like name, id, last updated and so on
// and it would have a RobotPosition variable instead of coordinate and orientation
struct RobotPosition: Hashable {
    let coordinate: Coordinate
    let orientation: Orientation
}

struct RobotInstruction {
    let robot: Robot
    let instructions: [Direction]
    
    init(with robot: Robot, instructions: String) {
        self.robot = robot
        self.instructions = instructions.compactMap { instruction -> Direction? in
            // here we choose to ignore the instructions that don't map to a Direction
            return Direction(rawValue: String(instruction))
        }
    }
}

class RobotManager {
    
    private let topRightCorner: Coordinate
    private var robotInstructions: [RobotInstruction] = []
    private var robotScents: [RobotPosition] = []
    
    init(with topRightCorner: Coordinate) {
        self.topRightCorner = topRightCorner
    }
    
    func add(robot: Robot, withInstructions instructions: String) {
        let robotInstruction = RobotInstruction(with: robot, instructions: instructions)
        robotInstructions.append(robotInstruction)
    }
    
    func computeInstructions() -> String {
        let result = try? robotInstructions.map { robotInstruction -> String in
            do {
                // position of the robot before executing instructions
                let initialRobotPosition = RobotPosition(coordinate: robotInstruction.robot.coordinate,
                                                         orientation: robotInstruction.robot.orientation)
                let newPosition = try robotInstruction.instructions.reduce(initialRobotPosition) { robotPosition, direction -> RobotPosition in
                    let diff = robotPosition.orientation.isIncremental ? 1 : -1
                    switch direction {
                    case .forward where robotPosition.orientation.isHorizontal:
                        // we move the x coordinate
                        let newCoordinate = Coordinate(x: robotPosition.coordinate.x + diff,
                                                        y: robotPosition.coordinate.y)
                        return try validate(newCoordinate: newCoordinate,
                                            originalCoordinate: robotPosition.coordinate,
                                            orientation: robotPosition.orientation)
                    case .forward where robotPosition.orientation.isVertical:
                        // we move the y coordinate
                        let newCoordinate = Coordinate(x: robotPosition.coordinate.x,
                                                        y: robotPosition.coordinate.y + diff)
                        return try validate(newCoordinate: newCoordinate,
                                            originalCoordinate: robotPosition.coordinate,
                                            orientation: robotPosition.orientation)
                    case .left:
                        // we update the orientation
                        return RobotPosition(coordinate: robotPosition.coordinate,
                                             orientation: robotPosition.orientation.rotated(isClockwise: false))
                    case .right:
                        // we update the orientation
                        return RobotPosition(coordinate: robotPosition.coordinate,
                                             orientation: robotPosition.orientation.rotated(isClockwise: true))
                    default:
                        // when we add more instructions, like diagonal movement for example
                        // we will crash the app to remind us to handle them in the Switch
                        fatalError("Unsupported instruction")
                    }
                }
                return "\(newPosition.coordinate.stringValue) \(newPosition.orientation.rawValue)"
            } catch let error as String {
                return error
            }
        }
        
        guard let robotPositions = result else {
            // our map - reduce should always return a String
            // if we crash here it means we modified the inner working of the map - reduce by mistake
            fatalError("This shouldn't happen")
        }
        return robotPositions.joined(separator: "\n")
    }
    
    func validate(newCoordinate: Coordinate, originalCoordinate: Coordinate, orientation: Orientation) throws -> RobotPosition {
        let robotPosition: RobotPosition = RobotPosition(coordinate: originalCoordinate,
                                                         orientation: orientation)
        
        guard !robotScents.contains(robotPosition) else {
            // we've already seen that out of bonds direction so we ignore the command
            return robotPosition
        }
        
        guard
            !newCoordinate.isGreater(than: topRightCorner),
            newCoordinate.x >= 0,
            newCoordinate.y >= 0,
            newCoordinate.x <= 50,
            newCoordinate.y <= 50
        else {
            // if it's the first robot to get lost here we record its last position
            robotScents.append(robotPosition)
            // once a robot is lost, it can't continue receiving instructions
            throw "\(originalCoordinate.stringValue) \(orientation.rawValue) LOST"
        }
        
        // the new position is valid, we can return it
        return RobotPosition(coordinate: newCoordinate, orientation: orientation)
    }
    
}
