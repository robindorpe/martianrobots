//
//  Robot.swift
//  MartianRobots
//
//  Created by Robin Dorpe on 10/03/2020.
//  Copyright © 2020 Coding Cube Ltd. All rights reserved.
//

import UIKit

enum Direction: String {
    case forward = "F"
    case left = "L"
    case right = "R"
}

enum Orientation: String, CaseIterable, Hashable {
    case north = "N"
    case south = "S"
    case east = "E"
    case west = "W"
    
    private static var horizontalOrientations: [Orientation] = [.east, .west]
    private static var verticalOrientations: [Orientation] = [.north, .south]
    private static var incrementalOrientations: [Orientation] = [.north, .east]
    
    var isHorizontal: Bool {
        return Orientation.horizontalOrientations.contains(self)
    }
    
    var isVertical: Bool {
        return Orientation.verticalOrientations.contains(self)
    }
    
    var isIncremental: Bool {
        return Orientation.incrementalOrientations.contains(self)
    }
    
    var title: String {
        return rawValue
    }
    
     func rotated(isClockwise: Bool) -> Orientation {
        switch self {
        case .north:
            return isClockwise ? .east : .west
        case .south:
            return isClockwise ? .west : .east
        case .east:
            return isClockwise ? .south : .north
        case .west:
            return isClockwise ? .north : .south
        }
    }
}

struct Coordinate: Hashable {
    let x: Int
    let y: Int
    
    var stringValue: String {
        return "\(x) \(y)"
    }
    
    func isGreater(than coordinate: Coordinate) -> Bool {
        return x > coordinate.x || y > coordinate.y
    }
}

class Robot {
    
    let coordinate: Coordinate
    var orientation: Orientation
    
    init(coordinate: Coordinate,
         orientation: Orientation) {
        self.coordinate = coordinate
        self.orientation = orientation
    }
}
