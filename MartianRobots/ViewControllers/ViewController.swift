//
//  ViewController.swift
//  MartianRobots
//
//  Created by Robin Dorpe on 09/03/2020.
//  Copyright © 2020 Coding Cube Ltd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var xCoordinateTextField: UITextField!
    @IBOutlet weak var yCoordinateTextField: UITextField!
    @IBOutlet weak var cardinalDirectionTextField: UITextField!
    @IBOutlet weak var instructionsTextField: UITextField!
    
    let pickerData: [Orientation] = Orientation.allCases
    
    // in prod app this would have to be set somewhere in the UI
    static let topRightCorner: Coordinate = Coordinate(x: 5, y: 3)
    // we initialise the robot manager with the top right corner
    // in a prod app we would have to initialise it once user input the top right corner manually
    var robotManager: RobotManager = RobotManager(with: topRightCorner)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // the cardinal text field uses a picker as an input view
        let pickerView: UIPickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        cardinalDirectionTextField.inputView = pickerView
        
        // the instruction text field uses a custom view as an input view
        let instructionView = InstructionInputView.fromNib()
        instructionView.delegate = self
        instructionsTextField.inputView = instructionView
    }
    
    func resetViews() {
        xCoordinateTextField.text = nil
        xCoordinateTextField.becomeFirstResponder()
        yCoordinateTextField.text = nil
        cardinalDirectionTextField.text = nil
        (cardinalDirectionTextField.inputView as? UIPickerView)?.selectRow(0, inComponent: 0, animated: false)
        instructionsTextField.text = nil
    }
    
    func validateInput() -> Bool {
        let isValid: Bool =
            xCoordinateTextField.isValid &&
            yCoordinateTextField.isValid &&
            cardinalDirectionTextField.isValid &&
            instructionsTextField.isValid
        
        if !isValid {
            // if the input fields are not all filled correctly, we display an error to the user
            let alertController = UIAlertController(title: "Missing Input", message: "Please fill all input before adding a new robot", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
            present(alertController, animated: true, completion: nil)
        }
        
        return isValid
    }

    @IBAction func addRobot(_ sender: UIButton) {
        guard
            validateInput(),
            let xCoordinateText = xCoordinateTextField.text,
            let xCoordinate = Int(xCoordinateText),
            xCoordinate <= ViewController.topRightCorner.x,
            let yCoordinateText = yCoordinateTextField.text,
            let yCoordinate = Int(yCoordinateText),
            yCoordinate <= ViewController.topRightCorner.y,
            let orintationText = cardinalDirectionTextField.text,
            let orientation = Orientation(rawValue: orintationText),
            let instructions = instructionsTextField.text
        else {
            return
        }
        // if input is valid we can add the robot
        let robot = Robot(coordinate: Coordinate(x: xCoordinate, y: yCoordinate),
                          orientation: orientation)
        robotManager.add(robot: robot, withInstructions: instructions)
        resetViews()
    }
    
    @IBAction func removeAllRobots(_ sender: UIButton) {
        robotManager = RobotManager(with: ViewController.topRightCorner)
    }
    
    @IBAction func computeInstructions(_ sender: UIButton) {
        let robotPositions = robotManager.computeInstructions()
        // in a prod app we would display the result to the user
        // here we log the result to the console
        print(robotPositions)
    }
    
}

extension ViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard
            let text = textField.text,
            let textRange = Range(range, in: text)
        else {
            return false
        }
        var updatedText = text.replacingCharacters(in: textRange, with: string)
        
        guard !updatedText.isEmpty else {
            // if the text is an empty string, we bypass the checks
            textField.text = updatedText
            return false
        }
        
        switch textField {
        case xCoordinateTextField, yCoordinateTextField:
            // only integers and not above 50
            updatedText = updatedText.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789").inverted)
            guard let coordinate = Int(updatedText), coordinate <= 50 else { return false }
        case cardinalDirectionTextField:
            // picker handles cardinal change
            return false
        case instructionsTextField where updatedText.count < 100:
            // we only allow L, R, F
            updatedText = updatedText.trimmingCharacters(in: CharacterSet(charactersIn: "LRF").inverted)
        default:
            return false
        }
        textField.text = updatedText
        
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard
            textField == cardinalDirectionTextField,
            let pickerView = textField.inputView as? UIPickerView
        else {
            return
        }
        updateCardinalTextFieldText(for: pickerView.selectedRow(inComponent: 0))
    }
}

extension ViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
}

extension ViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row].title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // we update the cardinal text field
        updateCardinalTextFieldText(for: row)
    }
    
    func updateCardinalTextFieldText(for row: Int) {
        cardinalDirectionTextField.text = pickerData[row].title
    }
    
}

extension ViewController: InstructionInputViewDelegate {
    
    func instructionInputView(_ view: InstructionInputView, didChooseDirection direction: Direction) {
        let instructionText = instructionsTextField.text ?? ""
        guard instructionText.count < 99 else { return }
        instructionsTextField.text = "\(instructionText)\(direction.rawValue)"
    }
    
    func instructionInputViewDidChooseDelete(_ view: InstructionInputView) {
        instructionsTextField.deleteBackward()
    }
    
}

