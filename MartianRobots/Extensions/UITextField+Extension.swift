//
//  UITextField+Extension.swift
//  MartianRobots
//
//  Created by Robin Dorpe on 10/03/2020.
//  Copyright © 2020 Coding Cube Ltd. All rights reserved.
//

import UIKit

extension UITextField {
    
    var isValid: Bool {
        guard let text = text, !text.isEmpty else {
            return false
        }
        return true
    }
    
}
