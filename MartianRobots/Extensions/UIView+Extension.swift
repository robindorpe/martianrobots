//
//  UIView+Extension.swift
//  MartianRobots
//
//  Created by Robin Dorpe on 10/03/2020.
//  Copyright © 2020 Coding Cube Ltd. All rights reserved.
//

import UIKit

extension UIView {
    
    static func fromNib() -> Self {
        return fromNib(withType: self)
    }
    
    private static func fromNib<T: UIView>(withType type: T.Type) -> T {
        return Bundle.main.loadNibNamed(String(describing: type), owner: nil, options: nil)![0] as! T
    }
    
}
